from Controller import Control
from Setup import Setup
import states


def main():
    print("----INICIANDO JUEGO----")
    Setup()
    print("----Setup finalized----")
    
    diccinoarioDeEstados = {
        "PantallaInicial" : states.PantallaInicio("Pantalla Inicio", "historia"),
        "historia" : states.history("Menu Principal", "MenuPrincipal"),
        "MenuPrincipal" : states.MenuPrincipal("Menu Principal", "Level1Tutorial"),
        "Level1Tutorial" : states.Level1Tutorial("level prueba", "Prueba"),
        "Prueba" : states.prologoPrueba("Prueba", "Interludio1"),
        "Interludio1" : states.Interludio1("Interludio", "PantallaInicial"),
        "LevelPlatform" : states.LevelPlatform("Prefacio Level", "Interludio2"),
        "Interludio2" : states.Interludio2("Interludio", "Level3"),
        "Level3" : states.Level3("Level 3", "PantallaInicial"),
        "GameOver" : states.GameOver("GameOver", "PantallaInicial"),
        "credits" : states.creditos("GameOver", "MenuPrincipal"),
        "tutorial" : states.TutorialTeclas("GameOver", "MenuPrincipal"),
        "Final" : states.LevelFinal("GameOver", "MenuPrincipal"),
    }


    controlador = Control()
    controlador.preparar_estados(diccinoarioDeEstados, diccinoarioDeEstados["MenuPrincipal"])
    print("Diccionario de estados prearados")
    controlador.main()
    print("FIN DEL JUEGO!")