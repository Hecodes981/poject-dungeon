import pygame as pg
import Components as comp

window_size = (980, 605)
CAPTION = "Default"

#Colores
ROJO = (255,0,0)
VERDE = (0,255,0)
AZUL = (0,0,255)
BLANCO = (255,255,255)
NEGRO = (0,0,0)
AMARILLO = (255,255,0)
CELESTE = (66, 244, 232)

def recortarImagenes(img, size, scale):
    img_rect = img.get_rect()[2:]
    matriz = []
    filas = img_rect[1]/size[1]
    columnas = img_rect[0]/size[0]

    for i in range(filas):
        matriz.append([])
        for j in range(columnas):
            frame = img.subsurface((j*size[0]), i*size[1], size[0], size[1])
            #frame = pg.transform.scale(frame, (scale * size[0], scale * size[1]))
            matriz[i].append(frame)
    return matriz

def fade(): 
    fade = pg.Surface(window_size)
    fade.fill((0,0,0))
    for alpha in range(0, 300):
        fade.set_alpha(alpha)
        redrawWindow()
        win.blit(fade, (0,0))
        pg.display.update()
        pg.time.delay(5)


def limpiarGrupos():
    for i in Grupos:
        Grupos[i].empty()

Grupos = {
"todos" : pg.sprite.LayeredUpdates(),
"player" : pg.sprite.Group(),
"muros"  : pg.sprite.Group(),
"shoots" : pg.sprite.Group(),
"shootsEnemies" : pg.sprite.Group(),
"weapons" : pg.sprite.Group(),
"collisions" : pg.sprite.Group(),
"bow" : pg.sprite.Group(),
"spear" : pg.sprite.Group(),
"enemy" : pg.sprite.Group(),
"fish" : pg.sprite.Group(),
"foxy" : pg.sprite.Group(),
"pato" : pg.sprite.Group(),
"bicho" : pg.sprite.Group(),
"enemigos" : pg.sprite.Group(),
"enemies" : pg.sprite.Group(),
"consumibles" : pg.sprite.Group(),
"consumiblesMana" : pg.sprite.Group(),
"gemas" : pg.sprite.Group(),

}


playerHacks = {
    "Nivel" : 1,
    "Exp" : 0
}

#Diccionarios de Recursos

graficosPantallaPrincipal = {
"LogoUTP" : pg.image.load("Recursos/Graficos/PantallaInicio/logoutp.png"),
"LogoISC" : pg.image.load("Recursos/Graficos/PantallaInicio/logoisc.png")
}

playerSheetsBow = {
    "Walk": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Move_bow.png"), (64, 64), 0.98),
    "attack": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Attack_bow.png"), (64, 64), 0.98),
}

playerSheetsDaga = {
    "Walk": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Move_daga.png"), (64, 64), 0.98),
    "attack": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Attack_daga.png"), (64, 64), 0.98),
}

playerSheetsSpear = {
    "Walk": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Move_spear.png"), (64, 64), 0.98),
    "attack": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Aragorn/Attack_spear.png"), (64, 64), 0.98),
}

EnemieZombie = {
    "live": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombieaparece.png"), (32, 32), 1.7), 
    "Walk":comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombiemovimiento.png"), (40, 38), 1.6),
    "idle":comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombiemovimiento.png"), (40, 38), 1.6),
    "attack":comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombievomito.png"), (90, 80), 1.5),
    "hit":comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombieherido.png"), (22, 40), 1.5),
    "Die":comp.recortarImagenes(pg.image.load("Recursos/Sprites/Zombie/zombiemuere.png"), (31, 19), 1.5),
    
}

EnemyJelly = {
    "ilde" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/JellyFish/pulpoidle.png"), (30, 30), 1.3),
    "Walk" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/JellyFish/pulpowalk.png"), (20, 30), 1.3), 
    "attack" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/JellyFish/pulpoattack.png"), (32, 32), 1.3),     
}


Enemy = {
    "idle" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Pato/patoidle.png"), (32,32), 1.7),
    "live" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Pato/patoaparece.png"), (30,30), 1.7),
    "Walk" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Pato/patoMove.png"), (32,32), 1.7),
    "hit" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Pato/patoHerido.png"), (30,30), 1.7),
    "attack" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Pato/patoattack.png"), (32,32), 1.7),    
}

Foxy = {
    "Walk" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/foxyOr/move.png"), (30,30), 1.7),
    "idle" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/foxyOr/move.png"), (30,30), 1.7),
    "attack" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/foxyOr/attack.png"), (35,30), 1.7),
    "hit" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/foxyOr/hit.png"), (24,30), 1.7),
    "die" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/foxyOr/die.png"), (30,24), 1.7),
}

Bicho = {
    "Walk" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Bicho/bichoMove.png"), (34,34), 1.6),
    "attack" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Bicho/bichoattack.png"), (34,34), 1.6),
    "hit" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Bicho/hit.png"), (30,24), 1.6),
    "death" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/Bicho/death.png"), (25,24), 1.6),
}

ItemSheets = {
    "BarLifePro": pg.image.load("Recursos/Graficos/Foregrounds/barlifepro.png"),
    "Hud": pg.image.load("Recursos/Graficos/Foregrounds/hud.png"),
    "Impacto1": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/impactonaranja.png"),(64,64),1),
    "Impacto": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/impactoazul.png"),(80,80),0.7),
    "Impactogrande": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/impactogrande.png"),(256,256),1),
    "arrow": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/flechaprincipal.png"),(17,17),1),
    "patoproyectil": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/patoproyectil.png"),(14,16),1),
    "arco": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/arco.png"),(34,34),1),
    "Spear": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/lanza.png"),(34,34),1),
    "consumible": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/pocionexp.png"),(34,34),1),
    "consumibleres": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/pocionres.png"),(34,34),1),
    "consumiblevida": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/carne.png"),(34,34),1),
    "gema": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/gema.png"),(34,34),1),
    "timer": pg.image.load("Recursos/Graficos/Foregrounds/Timer.png"),
    "flechaFoxy": comp.recortarImagenes(pg.image.load("Recursos/Sprites/Items/mapachenproyectil16x16.png"),(16,16),1)
}

GraficosMenu = {
    "Title":pg.image.load("Recursos/Menu/Titulo.png"),
    "backgroundMenu":pg.image.load("Recursos/Menu/MenuBackground.png"),
    "StartMenu":pg.image.load("Recursos/Menu/Jugar.png"),
    "CreditsMenu":pg.image.load("Recursos/Menu/Creditos.png"),
    "TutorialMenu":pg.image.load("Recursos/Menu/Tutorial.png"),
    "QuitMenu":pg.image.load("Recursos/Menu/salir.png"),
    "puntero":pg.image.load("Recursos/Menu/apuntadormenu.png"),
}

GameOver = {
    "Over":pg.image.load("Recursos/Menu/GameOver.png"),
}

Credits = {
    "credits":pg.image.load("Recursos/Creditos/Creditos.png"),
    "credits1":pg.image.load("Recursos/Creditos/creditos2.png"),
    "credits2":pg.image.load("Recursos/Creditos/creditos3.png"),

}

GraficosInterludios = {
    "backgroundInterludio1" : pg.image.load("Recursos/Graficos/Backgrounds/bg7.jpg"),
    "backgroundInterludio2":pg.image.load("Recursos/Menu/MenuBackground.png"),
    "backgroundInterludio3":pg.image.load("Recursos/Menu/interludio2.png"),
    "dialogo":pg.image.load("Recursos/Graficos/interludios/cuadro.png"),
    "dialogoA":pg.image.load("Recursos/Graficos/interludios/cuadroInverso.png"),
    "mago":pg.image.load("Recursos/Graficos/interludios/mago.png"),
    "alex":pg.image.load("Recursos/Graficos/interludios/alex.png"),
    "Lily":pg.image.load("Recursos/Graficos/interludios/Lilyy.png"),
    "historia1":pg.image.load("Recursos/Graficos/interludios/historia1.png"),
    "historia2":pg.image.load("Recursos/Graficos/interludios/historia2.png"),
    "historia3":pg.image.load("Recursos/Graficos/interludios/historia3.png"),
    "book":pg.image.load("Recursos/Graficos/interludios/book.png"),
}

pNJugables = {
    "Walk" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/no-jugables/max.png"), (32,32), 1.6),
    "WalkMago" : comp.recortarImagenes(pg.image.load("Recursos/Sprites/no-jugables/mago.png"), (32,32), 1.6),
}

LevelTutorial = {
    "Background" : pg.image.load("Recursos/Graficos/Backgrounds/bg1.png"),
    "Nube" : pg.image.load("Recursos/Graficos/Tutorial/nubetexto.png"),
}

LevelPrefacio = {
    "Background" : pg.image.load("Recursos/Graficos/Backgrounds/bg3.png")
}

Level3 = {
    "Background" : pg.image.load("Recursos/Graficos/Backgrounds/map1.png")
}

LevelFinal = {
    "Background" : pg.image.load("Recursos/Graficos/Backgrounds/FINAL.jpg")
}