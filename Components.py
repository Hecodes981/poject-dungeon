import pygame as pg
import Constants as c
import random

global_posicion_x = 0
global_posicion_y = 0

global_speed_x = 0
global_speed_y = 0

tick = 0

def recortarImagenes(img, size, scale = 1):
    img_rect = img.get_rect()[2:]
    matriz = []
    filas = img_rect[1]/size[1]
    columnas = img_rect[0]/size[0]

    for i in range(int(filas)):
        matriz.append([])
        for j in range(int(columnas)):
            frame = img.subsurface((j*size[0]), i*size[1], size[0], size[1])
            frame = pg.transform.scale(frame, (int(scale * size[0]), int(scale * size[1])))
            matriz[i].append(frame)
    return matriz

class Player(pg.sprite.Sprite):
    def __init__(self, tm, platform):
        pg.sprite.Sprite.__init__(self)
        self.dicAnimacion = c.playerSheetsDaga
        self.image = self.dicAnimacion["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.attack = self.image.get_rect()
        self.rect.x = 158
        self.rect.y = 212
        self.attack.x = 0
        self.attack.y = 0
        self.speedx = 0
        self.speedy = 0
        self.speed = 2
        self.indexAnim = 0
        self.limAnim = 0
        self.con = 0
        self.anim = "Walk"
        self.tick = 0
        self.tm = tm
        self.dir = 0
        self.limAttack = 0
        self.limx_attack = 0
        self.AttackValidate = False

        self.manaMax = 165
        self.mana = 165
        
        self.damageBow = 10
        
        self.spear = False
        self.bow = False

        self.vidaMax = 180
        self.vida = 180

        self.punorang = 20

        self.salto = False
        self.saltoCounter = 0
        self.platform = platform
        self.valueTick = 6
        self.damage = True
        self.danotick = 0

        self.hitBody = False
        self.hitSpear = False

        self.live = True
        self.vivito = True

        self.gravity = 0.4

        self.limSheet = 0

        self.Basedamage = 20

        self.exp = c.playerHacks["Exp"]
        self.nivel = c.playerHacks["Nivel"]
        self.expsiguientenivel = (2**self.nivel)*2+100
        self.resistencia = self.nivel*2
        self.damageBow = self.Basedamage*self.nivel/2 
 

    def subirnivel(self):
        self.exp = 0
        self.nivel +=1
        self.expsiguientenivel = (2**self.nivel)*2+100
        text = indicator((self.rect.centerx, self.rect.centery), "+{} Subiste de nivel".format(self.nivel), c.ROJO)
        c.Grupos["todos"].add(text)
        self.damageBow = self.Basedamage*self.nivel/2
        self.resistencia = self.nivel*2
        self.vida = self.vidaMax

    def disparar(self, xy,dir):
        b = Shoot(xy, dir, self.damageBow, 1)
        c.Grupos["shoots"].add(b)
        c.Grupos["todos"].add(b)

    def hit(self):
        pospuno = [0,0]
        if self.dir == 0:
            pospuno = [self.rect.centerx,self.rect.bottom + self.punorang]
        elif self.dir == 3:
            pospuno = [self.rect.right + self.punorang,self.rect.centery]
        elif self.dir == 1:
            pospuno = [self.rect.centerx ,self.rect.top - self.punorang]
        elif self.dir == 2:
            pospuno = [self.rect.left - self.punorang,self.rect.centery]

        if self.hitBody:
            i = Impacto(pospuno,1)
            d = self.damageBow
            print("estoy atacando con daga")
        elif self.hitSpear:
            i = Impacto(pospuno, 0)
            d = self.damageBow
            print("estoy atacando con Lanza")

        c.Grupos["todos"].add(i)
        for en in c.Grupos["enemigos"]:
            if en.live:
                collide = pg.sprite.collide_circle(i, en)
                if collide:
                    if self.hitBody or self.hitSpear:
                        text = indicator((en.rect.centerx, en.rect.centery), "-{} hp".format(d), c.ROJO)
                        c.Grupos["todos"].add(text)
                    en.vida -= d
        


    def update(self):

        global global_posicion_x
        global global_posicion_y
        global global_speed_x
        global global_speed_y
        self.xspeed = 0
        self.yspeed = 0


        keystate = pg.key.get_pressed()

        if self.platform:
            if not keystate[pg.K_RIGHT] and not keystate[pg.K_LEFT] and not keystate[pg.K_UP] and not keystate[pg.K_DOWN]:
                self.speedx = 0

            if not self.AttackValidate:
                self.limAnim = 0
        
            if not keystate[pg.K_b]:
                self.limAttack = 0
            
            if not keystate[pg.K_SPACE]:
                self.salto = False

            if not keystate[pg.K_LSHIFT]:
                self.speed = 2
                self.valueTick = 6


            if keystate[pg.K_SPACE] and not keystate[pg.K_LEFT] and not keystate[pg.K_RIGHT]:
                self.salto = True
                self.flagJump = True

            if keystate[pg.K_SPACE] and keystate[pg.K_RIGHT]:
                self.speedx = self.speed
                self.salto = True
                self.flagJump = True


            if (keystate[pg.K_SPACE] and keystate[pg.K_LEFT]) :
                self.speedx = -self.speed
                self.salto = True
                self.flagJump = True

            if keystate[pg.K_RIGHT] and not self.AttackValidate and not self.salto:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 3
                self.limAnim = 8
                self.speedx = self.speed
                self.dir = 3
            if keystate[pg.K_LEFT] and not self.AttackValidate and not self.salto:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 2
                self.limAnim = 8
                self.speedx = -self.speed
                self.dir = 2

            if keystate[pg.K_LSHIFT]:
                self.mana -= 0.1


                self.speed = 3
                self.valueTick = 3
                if self.tick > self.valueTick:
                    self.tick = 0


            if keystate[pg.K_b] and self.limAttack == 0 and not self.AttackValidate and not keystate[pg.K_RIGHT] and not keystate[pg.K_LEFT]:
                if self.bow:
                    self.limSheet = 7
                    self.limAnim = 13
                else:
                    if self.spear:
                        self.hitBody = False
                        self.hitSpear = True
                        self.limSheet = 6
                        self.limAnim = 7
                        self.hit()
                    else:
                        self.hitSpear = False
                        self.hitBody = True
                        self.limSheet = 5
                        self.limAnim = 6
                        self.hit()

                
                self.speedx = 0
                self.speedx = 0
                self.anim = "attack"            
                self.AttackValidate = True
                self.limAttack = 1
                self.con = 0

                if self.dir == 1:
                    self.indexAnim = 2
                if self.dir == 3:
                    self.indexAnim = 3
                    
            if self.anim == "attack" and self.con == self.limSheet:
                if self.bow:
                    self.disparar(self.rect.center, self.dir)
                self.AttackValidate = False
                self.limAnim = 0
                self.con = 0
                if self.dir == 1:
                    self.anim = "Walk"
                    self.indexAnim = 2
                if self.dir == 3:
                    self.anim = "Walk"
                    self.indexAnim = 3

        
        if not self.platform:
            if not keystate[pg.K_RIGHT] and not keystate[pg.K_LEFT] and not keystate[pg.K_UP] and not keystate[pg.K_DOWN]:
                self.speedx = 0
                self.speedy = 0
                if not self.AttackValidate:
                    self.limAnim = 0
            
            if not keystate[pg.K_b]:
                self.limAttack = 0

            if not keystate[pg.K_LSHIFT]:
                self.speed = 2
                self.valueTick = 6

            if keystate[pg.K_RIGHT] and not self.AttackValidate:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 3
                self.limAnim = 8
                self.speedx = self.speed
                self.speedy = 0
                self.dir = 3
            if keystate[pg.K_LEFT] and not self.AttackValidate:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 2
                self.limAnim = 8
                self.speedx = -self.speed
                self.speedy = 0
                self.dir = 2
            if keystate[pg.K_DOWN] and not self.AttackValidate:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 0
                self.limAnim = 8
                self.speedy = self.speed
                self.speedx = 0
                self.dir = 0
            if keystate[pg.K_UP] and not self.AttackValidate:
                self.limAnim = 0
                self.anim = "Walk"
                self.indexAnim = 1
                self.limAnim = 8
                self.speedy = -self.speed
                self.speedx = 0
                self.dir = 1

            if keystate[pg.K_LSHIFT]:
                
                if self.mana > 1:
                    self.mana -= 0.1
                    self.speed = 3
                    self.valueTick = 3
                    if self.tick > self.valueTick:
                        self.tick = 0
                else: 
                    self.mana = 0
                    self.speed = 2
                    self.valueTick = 6
                

            if keystate[pg.K_b] and self.limAttack == 0 and not self.AttackValidate and not keystate[pg.K_DOWN] and not keystate[pg.K_RIGHT] and not keystate[pg.K_LEFT] and not keystate[pg.K_UP]:
                if self.bow:
                    self.limSheet = 7
                    self.limAnim = 13
                else:
                    if self.spear:
                        self.hitBody = False
                        self.hitSpear = True
                        self.limSheet = 6
                        self.limAnim = 7
                        self.hit()
                    else:
                        self.hitSpear = False
                        self.hitBody = True
                        self.limSheet = 5
                        self.limAnim = 6
                        self.hit()
                
                self.speedx = 0
                self.speedx = 0
                self.anim = "attack"            
                self.AttackValidate = True
                self.limAttack = 1
                self.con = 0

                if self.dir == 0:
                    self.indexAnim = 0
                if self.dir == 1:
                    self.indexAnim = 1
                if self.dir == 2:
                    self.indexAnim = 2
                if self.dir == 3:
                    self.indexAnim = 3
                    
            if self.anim == "attack" and self.con == self.limSheet:
                if self.bow:
                    self.disparar(self.rect.center, self.dir)
                self.AttackValidate = False
                self.limAnim = 0
                self.con = 0
                if self.dir == 0 or self.dir == 4:
                    self.anim = "Walk"
                    self.indexAnim = 0
                if self.dir == 1:
                    self.anim = "Walk"
                    self.indexAnim = 1
                if self.dir == 2:
                    self.anim = "Walk"
                    self.indexAnim = 2
                if self.dir == 3:
                    self.anim = "Walk"
                    self.indexAnim = 3

    #GESTION DE ANIMACIONES
        if self.tick == self.valueTick:
            if self.con < self.limAnim:
                self.con+=1
                self.tick = 0
            else:
                self.con = 0
                self.tick = 0

        self.image = self.dicAnimacion[self.anim][self.indexAnim][self.con]


        self.rect.x += self.speedx  
        self.rect.y += self.speedy
        self.tick+=1

        #LIMITES GLOBALES

        if self.platform:    
            if abs(global_posicion_x - c.window_size[0]) < self.tm[0]:
                if self.rect.right > c.window_size[0] - 250:
                    self.rect.right = c.window_size[0] - 250
                    global_posicion_x -= self.speed
                    global_speed_x = -self.speed
                else:
                    global_speed_x = 0
            else:
                if self.rect.right > c.window_size[0] - 32:
                    self.rect.right = c.window_size[0] - 32
                global_speed_x = 0

            if global_posicion_x <= 0:
                if self.rect.left < 156:
                    self.rect.left = 156
                    global_posicion_x += self.speed
                    global_speed_x = self.speed

            else:
                if self.rect.left < 32:
                    self.rect.left = 32
                global_speed_x = 0

        if not self.platform:

            if abs(global_posicion_x - c.window_size[0]) < self.tm[0]:
                if self.rect.right > c.window_size[0] - 200:
                    self.rect.right = c.window_size[0] - 200
                    global_posicion_x -= self.speed
                    global_speed_x = -self.speed
                else:
                    global_speed_x = 0
            else:
                if self.rect.right > c.window_size[0] - 32:
                    self.rect.right = c.window_size[0] - 32
                global_speed_x = 0

            if global_posicion_x <= 0:
                if self.rect.left < 156:
                    self.rect.left = 156
                    global_posicion_x += self.speed
                    global_speed_x = self.speed

            else:
                if self.rect.left < 32:
                    self.rect.left = 32
                global_speed_x = 0


            if abs(global_posicion_y - c.window_size[1]) < self.tm[1]:
                if self.rect.bottom > c.window_size[1] - 140:
                    self.rect.bottom = c.window_size[1] - 140
                    global_posicion_y -= self.speed
                    global_speed_y = -self.speed
                else:
                    global_speed_y = 0

            else:
                if self.rect.bottom > c.window_size[1] - 128:
                    self.rect.bottom = c.window_size[1] -128
                global_speed_y = 0

            if global_posicion_y <= 0:
                if self.rect.top < 100:
                    self.rect.top = 100
                    global_posicion_y += self.speed
                    global_speed_y = self.speed
            else:
                if self.rect.top < 32:
                    self.rect.top = 32
                global_speed_y = 0

        if self.platform:

            for en in c.Grupos["enemies"]:
                collide = pg.sprite.collide_circle(self,en)
                if collide:
                    if self.damage:
                        totalDamage = (en.cdano - (self.resistencia * en.cdano / 100))
                        text = indicator((en.rect.centerx, en.rect.centery), "-{} hp".format(totalDamage), c.VERDE)
                        c.Grupos["todos"].add(text)
                        self.vida -= totalDamage

            self.danotick+=1
            if self.danotick == 30:
                self.danotick = 0
                self.damage = True
            else:
                self.damage = False
            
            collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

            if self.speedy == 0:
                self.speedy=0.5
            else:
                self.speedy+=self.gravity

            if self.salto and self.saltoCounter < 10:
                self.speedy = -7
                self.salto = False
                self.saltoCounter+=1

            collideSpear = pg.sprite.spritecollide(self, c.Grupos["spear"], True)

            for spear in collideSpear:
                self.dicAnimacion = c.playerSheetsSpear
                self.bow = False
                self.spear = True

            if self.vida <= 0:
                # self.live = False
                self.vivito  =  False

            for muro in collision:
                #if self.speedx > 0 and self.speedy == 0:
                #   self.rect.right = muro.rect.left
                #elif self.speedx < 0 and self.speedy == 0:
                #   self.rect.left = muro.rect.right

                #if self.speedy > 0 and self.speedx == 0:
                #   self.rect.bottom = muro.rect.top
                #elif self.speedy < 0 and self.speedx == 0:
                #   self.rect.top = muro.rect.bottom
                if self.rect.bottom >= muro.rect.top and self.speedy > 0:
                    self.rect.bottom = muro.rect.top
                    self.saltoCounter=0
                    self.speedy = 0

            if self.rect.y > c.window_size[1]:
                self.rect.x = 0
                global_posicion_x = 0
                self.live = False

        if not self.platform:
            collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

            for muro in collision:
                if self.speedx > 0 and self.speedy == 0:
                    self.rect.right = muro.rect.left
                elif self.speedx < 0 and self.speedy == 0:
                    self.rect.left = muro.rect.right

                if self.speedy > 0 and self.speedx == 0:
                    self.rect.bottom = muro.rect.top
                elif self.speedy < 0 and self.speedx == 0:
                    self.rect.top = muro.rect.bottom

            
            #Collisiones con el enemigo

            collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shootsEnemies"], False)

            for b in collisionShoot:
                im = Impacto(self.rect.center, 0)
                c.Grupos["todos"].add(im)
                totalDamage = (b.damage - (self.resistencia * b.damage / 100))
                self.vida-=totalDamage
                text = indicator((b.rect.centerx, b.rect.centery), "-{} hp".format(totalDamage), c.VERDE)
                c.Grupos["todos"].add(text)

                b.kill()
            # if self.vida <= 0:
            #     self.prueba+=1
            #     self.live = False
            #     self.reward()
            #     self.kill()


            for en in c.Grupos["enemies"]:
                collide = pg.sprite.collide_circle(self,en)
                if collide:
                    if self.damage:
                        totalDamage = (en.cdano - (self.resistencia * en.cdano / 100))
                        text = indicator((en.rect.centerx, en.rect.centery), "-{} hp".format(totalDamage), c.VERDE)
                        c.Grupos["todos"].add(text)
                        self.vida -= totalDamage    

            self.danotick+=1
            if self.danotick == 30:
                self.danotick = 0
                self.damage = True
            else:
                self.damage = False

            
            collisionCons = pg.sprite.spritecollide(self, c.Grupos["consumibles"], True)

            for consumible in collisionCons:
                if consumible.tipo == "Exp":
                    self.exp +=17
                    text = indicator((consumible.rect.centerx, consumible.rect.centery), "+{} exp".format(17), c.ROJO)
                    c.Grupos["todos"].add(text)
                    if self.exp >= self.expsiguientenivel:
                        self.subirnivel()
                if consumible.tipo == "Resistencia":
                    if self.mana >= self.manaMax:
                        text = indicator((consumible.rect.centerx, consumible.rect.centery), "+{} / Full mana".format(20), c.AZUL)
                        c.Grupos["todos"].add(text)
                        self.mana = self.manaMax

                    else:
                        self.mana +=20
                        text = indicator((consumible.rect.centerx, consumible.rect.centery), "+{} Mana".format(20), c.ROJO)
                        c.Grupos["todos"].add(text)
                if consumible.tipo == "Vida":

                    if self.vida+20 >= self.vidaMax:
                        text = indicator((consumible.rect.centerx, consumible.rect.centery), "+{} / Full Vida".format(20), c.AZUL)
                        c.Grupos["todos"].add(text)
                        self.vida = self.vidaMax
                    else:
                        text = indicator((consumible.rect.centerx, consumible.rect.centery), "+{} Vida".format(20), c.ROJO)
                        c.Grupos["todos"].add(text)
                        self.vida+=20


            

            

            collideWeapons = pg.sprite.spritecollide(self, c.Grupos["bow"], True)

            for bow in collideWeapons:
                self.dicAnimacion = c.playerSheetsBow
                self.bow = True
                self.spear = False

            collideSpear = pg.sprite.spritecollide(self, c.Grupos["spear"], True)

            for spear in collideSpear:
                self.dicAnimacion = c.playerSheetsSpear
                self.bow = False
                self.spear = True

            if self.vida <= 0:
                # self.live = False
                self.vivito  =  False


class Shoot(pg.sprite.Sprite):
    def __init__(self, xy, dir, damage, t):
        pg.sprite.Sprite.__init__(self)

        self.tipo = t
        if self.tipo == 1:
            self.matrixAnimation = c.ItemSheets["arrow"]
        elif self.tipo == 0:
            self.matrixAnimation = c.ItemSheets["patoproyectil"]
        elif self.tipo == 2:
            self.matrixAnimation = c.ItemSheets["flechaFoxy"]


        # self.image = pg.Surface([30,10])
        self.image =  self.matrixAnimation[0][0]
        # self.image.fill(c.AZUL)
        self.rect = self.image.get_rect()
        self.speed = 4
        #self.dir = dir
        self.speedx = 0
        self.speedy = 0
        self.rect.centerx = xy[0]
        self.rect.centery = xy[1]
        self.dir = dir

        self.damage = damage

    def update(self):
        self.speedx = global_speed_x
        self.speedy = global_speed_y

        if self.dir == 0 or self.dir == 4:
            self.speedy = global_speed_y + self.speed
            # self.image = pg.Surface([10,30])
        if self.dir == 3:
            self.speedx = global_speed_x + self.speed
        if self.dir == 1:
            self.speedy = global_speed_y - self.speed
            # self.image = pg.Surface([10,30])
        if self.dir == 2:
            self.speedx = global_speed_x - self.speed


        self.rect.x += self.speedx
        self.rect.y += self.speedy

        self.image = self.matrixAnimation[self.dir][0]


        if self.rect.x > c.window_size[0] + 20 or self.rect.x < -20 or self.rect.y > c.window_size[1] + 20 or self.rect.y < -20:
            self.kill()


class Muro(pg.sprite.Sprite):
    def __init__(self, possize):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((possize[2],possize[3]))
        self.rect = self.image.get_rect()
        self.rect.x = possize[0]
        self.rect.y = possize[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y


class Bow(pg.sprite.Sprite):
    def __init__(self, possize):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.ItemSheets["arco"]
        self.image = self.matrixAnimation[0][0]
        self.rect = self.image.get_rect()
        self.rect.x = possize[0]
        self.rect.y = possize[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class Spear(pg.sprite.Sprite):
    def __init__(self, possize):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.ItemSheets["Spear"]
        self.image = self.matrixAnimation[0][0]
        self.rect = self.image.get_rect()
        self.rect.x = possize[0]
        self.rect.y = possize[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class Weapons(pg.sprite.Sprite):
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface((10,10))
        self.image.fill(c.ROJO)
        self.rect = self.image.get_rect()
        self.rect.x = 500
        self.rect.y = 300
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class Enemy(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.name = "Enemy"
        self.matrixAnimation = c.EnemieZombie
        self.image = self.matrixAnimation["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.xspeed = 0
        self.yspeed = 0
        self.speed = 2
        self.damage = 10
        self.live = True
        self.radius = 50
        self.cdano = 2

        self.disAttack = 100

        self.prueba  = 0

        self.playerDamageBow = 10

        self.vida = 80

        self.anim = "idle"
        self.dir = 0
        self.animlimit = 0

        self.timeidle = random.randrange(100,200)
        self.timerun = random.randrange(100,200)

        self.contrun = 0
        self.contidle = 0
        

        self.n = 0
        self.speedanim = 5
        self.indexanim = 0

        self.attackTick = 0
        self.damage = True

    def reward(self):
        n = random.randrange(3,6)
        for i in range(1):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), "Exp")
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
        for i in range(n):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            t = random.randrange(0,3)
            if t == 0:
                tipo = "Exp"
            elif t == 1:
                tipo = "Resistencia"
            elif t ==  2:
                tipo = "Vida"

            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), tipo)
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)

    def update(self):
        self.xspeed = global_speed_x
        self.yspeed = global_speed_y

        if self.live:
            if self.anim == "Walk":
                self.speed = 2
            else:
                self.speed = 0
            
            for us in c.Grupos["player"]:


                if (us.rect.bottom>self.rect.top) and (us.rect.right<self.rect.left):
                    self.dir = 2
                elif (us.rect.bottom>self.rect.top) and (us.rect.left>self.rect.right):
                    self.dir = 3
                elif (us.rect.bottom<self.rect.top):
                    self.dir = 1
                elif (us.rect.top>self.rect.bottom):
                    self.dir = 0
                else:
                    pass

                if self.dir == 0:
                    self.yspeed = global_speed_y + self.speed
                elif self.dir == 3:
                    self.xspeed = global_speed_x + self.speed
                elif self.dir == 1:
                    self.yspeed = global_speed_y - self.speed
                elif self.dir == 2:
                    self.xspeed = global_speed_x - self.speed
                else:
                    pass

                
                

                self.rect.x += self.xspeed
                self.rect.y += self.yspeed

                # if self.contidle < self.timeidle:
                #     self.contidle+=1
                #     self.anim = "idle"
                # elif self.contrun < self.timerun:
                #     self.contrun+=1
                #     self.anim = "Walk"
                # else:
                #     self.anim = "attack"

                for player in c.Grupos["player"]:
                    # collide = pg.sprite.collide_circle(self,player)
                    # if collide:
                    #     self.anim = "attack"

                    collidePrueba = pg.sprite.collide_rect_ratio(0.7)(self, player)                    
                    if collidePrueba:
                        self.anim = "attack"
                    else:
                        collidePrueba = pg.sprite.collide_rect_ratio(2.0)(self, player)                    
                        if collidePrueba:
                            self.anim = "Walk"
                        else:
                            self.anim = "idle"

                    # collidePrueba = pg.sprite.collide_rect_ratio(1.0)(self, player)
                    # if collidePrueba:
                    #     print("holi")


                if self.n < self.speedanim:
                    self.n+=1
                else:
                    self.n = 0
                    if self.anim == "idle":
                        self.animlimit = 0
                    elif self.anim == "attack":
                        self.animlimit = 2
                    else:
                        self.animlimit = 1

                    if self.indexanim < self.animlimit:
                        self.indexanim+=1
                    else:
                        if self.anim == "attack":
                            self.contrun = 0
                            self.contidle = 0
                        self.indexanim = 0

                if self.anim == "idle":
                    self.image = self.matrixAnimation[self.anim][self.dir][0]
                elif self.anim == "attack":
                    self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                # elif self.anim == "hit":
                #     self.image = self.matrixAnimation[self.anim][0][0]
                elif self.anim == "Walk":
                    try:
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                    except IndexError:
                        self.indexanim = 0
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]


                collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

                for w in collision:
                    if self.dir == 0:
                        self.dir = 1
                        self.rect.y-=5
                    elif self.dir == 1:
                        self.dir = 0
                        self.rect.y+=5
                    elif self.dir == 2:
                        self.dir = 3
                        self.rect.x+=5
                    elif self.dir == 3:
                        self.dir = 2
                        self.rect.x -=5
                        
                collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shoots"], False)

                for b in collisionShoot:

                    im = Impacto(self.rect.center, 0)
                    c.Grupos["todos"].add(im)
                    b.kill()
                    self.vida-=b.damage
                if self.vida <= 0:
                    self.prueba+=1
                    self.live = False
                    self.image = self.matrixAnimation["Die"][0][0]
                    self.reward()
                    self.kill()


class Foxy(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.name = "Foxy"
        self.matrixAnimation = c.Foxy
        self.image = self.matrixAnimation["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.xspeed = 0
        self.yspeed = 0
        self.speed = 2
        self.damage = 10
        self.live = True
        self.radius = 50
        self.cdano = 2

        self.disAttack = 100

        self.prueba  = 0

        self.playerDamageBow = 8

        self.vida = 80

        self.anim = "Walk"
        self.dir = 0
        self.animlimit = 0

        self.timeidle = random.randrange(100,200)
        self.timerun = random.randrange(100,200)

        self.contrun = 0
        self.contidle = 0
        

        self.n = 0
        self.speedanim = 8
        self.indexanim = 0

        self.attackTick = 0
        self.damage = True

    def reward(self):
        n = random.randrange(3,6)
        for i in range(1):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), "Exp")
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
        for i in range(n):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            t = random.randrange(0,3)
            if t == 0:
                tipo = "Exp"
            elif t == 1:
                tipo = "Resistencia"
            elif t == 2:
                tipo = "Vida"

            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), tipo)
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)

    def disparar(self):
        b = Shoot(self.rect.center, self.dir, self.playerDamageBow, 2)
        c.Grupos["shootsEnemies"].add(b)
        c.Grupos["todos"].add(b)
    

    def update(self):
        self.xspeed = global_speed_x
        self.yspeed = global_speed_y

        if self.live:
            if self.anim == "Walk":
                self.speed = 2
            else:
                self.speed = 0
            
            for us in c.Grupos["player"]:


                if (us.rect.bottom>self.rect.top) and (us.rect.right<self.rect.left):
                    self.dir = 2
                elif (us.rect.bottom>self.rect.top) and (us.rect.left>self.rect.right):
                    self.dir = 3
                elif (us.rect.bottom<self.rect.top):
                    self.dir = 1
                elif (us.rect.top>self.rect.bottom):
                    self.dir = 0
                else:
                    pass

                if self.dir == 0:
                    self.yspeed = global_speed_y + self.speed
                elif self.dir == 3:
                    self.xspeed = global_speed_x + self.speed
                elif self.dir == 1:
                    self.yspeed = global_speed_y - self.speed
                elif self.dir == 2:
                    self.xspeed = global_speed_x - self.speed
                else:
                    pass

                
                

                self.rect.x += self.xspeed
                self.rect.y += self.yspeed

                # if self.contidle < self.timeidle:
                #     self.contidle+=1
                #     self.anim = "idle"
                # elif self.contrun < self.timerun:
                #     self.contrun+=1
                #     self.anim = "Walk"
                # else:
                #     self.anim = "attack"

                for player in c.Grupos["player"]:
                    # collide = pg.sprite.collide_circle(self,player)
                    # if collide:
                    #     self.anim = "attack"

                    collidePrueba = pg.sprite.collide_rect_ratio(2.0)(self, player)                    
                    if collidePrueba:
                        self.anim = "attack"
                    else:
                        collidePrueba = pg.sprite.collide_rect_ratio(3.0)(self, player)                    
                        if collidePrueba:
                            self.anim = "Walk"
                        else: 
                            self.anim = "idle"
                    # collidePrueba = pg.sprite.collide_rect_ratio(1.0)(self, player)
                    # if collidePrueba:
                    #     print("holi")


                if self.n < self.speedanim:
                    self.n+=1
                else:
                    self.n = 0

                    if self.anim == "idle":
                        self.animlimit = 0
                    elif self.anim == "attack":
                        self.animlimit = 2
                    else:
                        self.animlimit = 3

                    if self.indexanim < self.animlimit:
                        self.indexanim+=1
                    else:
                        if self.anim == "attack":
                            self.disparar()
                            self.contrun = 0
                            self.contidle = 0
                        self.indexanim = 0

                if self.anim == "idle":
                    self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                elif self.anim == "attack":
                    try:
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                    except IndexError:
                        self.indexanim = 0
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                # elif self.anim == "hit":
                #     self.image = self.matrixAnimation[self.anim][0][0]
                elif self.anim == "Walk":
                    try:
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                    except IndexError:
                        self.indexanim = 0
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]


                collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

                for w in collision:
                    if self.dir == 0:
                        self.dir = 1
                        self.rect.y-=5
                    elif self.dir == 1:
                        self.dir = 0
                        self.rect.y+=5
                    elif self.dir == 2:
                        self.dir = 3
                        self.rect.x+=5
                    elif self.dir == 3:
                        self.dir = 2
                        self.rect.x -=5
                        
                collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shoots"], False)

                for b in collisionShoot:

                    im = Impacto(self.rect.center, 0)
                    c.Grupos["todos"].add(im)
                    b.kill()
                    self.vida-=b.damage
                if self.vida <= 0:
                    self.prueba+=1
                    self.live = False
                    # self.image = self.matrixAnimation["Die"][0][0]
                    self.reward()
                    self.kill()

class Bicho(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.name = "Bicho"
        self.matrixAnimation = c.Bicho
        self.image = self.matrixAnimation["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.xspeed = 0
        self.yspeed = 0
        self.speed = 2
        self.damage = 8
        self.live = True
        self.radius = 50
        self.cdano = 2

        self.disAttack = 100

        self.prueba  = 0

        self.playerDamageBow = 10

        self.vida = 80

        self.anim = "Walk"
        self.dir = 0
        self.animlimit = 0

        self.timeidle = random.randrange(100,200)
        self.timerun = random.randrange(100,200)

        self.contrun = 0
        self.contidle = 0
        

        self.n = 0
        self.speedanim = 5
        self.indexanim = 0

        self.attackTick = 0
        self.damage = True

    def reward(self):
        n = random.randrange(3,6)
        for i in range(1):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), "Exp")
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
        for i in range(n):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            t = random.randrange(0,3)
            if t == 0:
                tipo = "Exp"
            elif t == 1:
                tipo = "Resistencia"
            elif t ==  2:
                tipo = "Vida"

            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), tipo)
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
    

    def update(self):
        self.xspeed = global_speed_x
        self.yspeed = global_speed_y

        if self.live:
            if self.anim == "Walk":
                self.speed = 2
            else:
                self.speed = 0
            
            for us in c.Grupos["player"]:


                if (us.rect.bottom>self.rect.top) and (us.rect.right<self.rect.left):
                    self.dir = 2
                elif (us.rect.bottom>self.rect.top) and (us.rect.left>self.rect.right):
                    self.dir = 3
                elif (us.rect.bottom<self.rect.top):
                    self.dir = 1
                elif (us.rect.top>self.rect.bottom):
                    self.dir = 0
                else:
                    pass

                if self.dir == 0:
                    self.yspeed = global_speed_y + self.speed
                elif self.dir == 3:
                    self.xspeed = global_speed_x + self.speed
                elif self.dir == 1:
                    self.yspeed = global_speed_y - self.speed
                elif self.dir == 2:
                    self.xspeed = global_speed_x - self.speed
                else:
                    pass

                
                

                self.rect.x += self.xspeed
                self.rect.y += self.yspeed

                # if self.contidle < self.timeidle:
                #     self.contidle+=1
                #     self.anim = "idle"
                # elif self.contrun < self.timerun:
                #     self.contrun+=1
                #     self.anim = "Walk"
                # else:
                #     self.anim = "attack"

                for player in c.Grupos["player"]:
                    # collide = pg.sprite.collide_circle(self,player)
                    # if collide:
                    #     self.anim = "attack"

                    collidePrueba = pg.sprite.collide_rect_ratio(0.7)(self, player)                    
                    if collidePrueba:
                        self.anim = "attack"
                    else:
                        collidePrueba = pg.sprite.collide_rect_ratio(7.0)(self, player)                    
                        if collidePrueba:
                            self.anim = "Walk"

                    # collidePrueba = pg.sprite.collide_rect_ratio(1.0)(self, player)
                    # if collidePrueba:
                    #     print("holi")


                if self.n < self.speedanim:
                    self.n+=1
                else:
                    self.n = 0
                    if self.anim == "attack":
                        self.animlimit = 8
                    else:
                        self.animlimit = 1

                    if self.indexanim < self.animlimit:
                        self.indexanim+=1
                    else:
                        if self.anim == "attack":
                            self.contrun = 0
                            self.contidle = 0
                        self.indexanim = 0

                if self.anim == "attack":
                    self.image = self.matrixAnimation[self.anim][0][self.indexanim]
                # elif self.anim == "hit":
                #     self.image = self.matrixAnimation[self.anim][0][0]
                elif self.anim == "Walk":
                    try:
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]
                    except IndexError:
                        self.indexanim = 0
                        self.image = self.matrixAnimation[self.anim][self.dir][self.indexanim]


                collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

                for w in collision:
                    if self.dir == 0:
                        self.dir = 1
                        self.rect.y-=5
                    elif self.dir == 1:
                        self.dir = 0
                        self.rect.y+=5
                    elif self.dir == 2:
                        self.dir = 3
                        self.rect.x+=5
                    elif self.dir == 3:
                        self.dir = 2
                        self.rect.x -=5
                        
                collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shoots"], False)

                for b in collisionShoot:

                    im = Impacto(self.rect.center, 0)
                    c.Grupos["todos"].add(im)
                    b.kill()
                    self.vida-=b.damage
                if self.vida <= 0:
                    self.prueba+=1
                    self.vivito = False
                    # self.image = self.matrixAnimation["Die"][0][0]
                    self.reward()
                    self.kill()

class Fish(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.name = "Fish"
        self.matrixAnimation = c.EnemyJelly
        self.image = self.matrixAnimation["Walk"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.xspeed = 0
        self.yspeed = 0
        self.speed = 2
        self.damage = 10
        self.live = True
        self.radius = 50
        self.cdano = 5

        self.disAttack = 150

        self.disAttack = 100

        self.prueba  = 0

        self.playerDamageBow = 10

        self.vida = 40

        self.anim = "Walk"
        self.dir = 0
        self.animlimit = 0

        self.timeidle = random.randrange(100,200)
        self.timerun = random.randrange(100,200)

        self.contrun = 0
        self.contidle = 0
        

        self.n = 0
        self.speedanim = 10
        self.indexanim = 0

        self.attackTick = 0
        self.damage = True

    def reward(self):
        n = random.randrange(3,6)
        for i in range(1):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), "Exp")
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
        for i in range(n):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            t = random.randrange(0,3)
            if t == 0:
                tipo = "Exp"
            elif t == 1:
                tipo = "Resistencia"
            elif t ==  2:
                tipo = "Vida"

            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), tipo)
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
    

    def update(self):
        self.xspeed = global_speed_x
        self.yspeed = global_speed_y

        if self.live:
            if self.anim == "Walk":
                self.speed = 2
            elif self.anim == "attack":
                self.speed = 2
            else: 
                self.speed = 0
            
            for us in c.Grupos["player"]:

                if (us.rect.right<self.rect.left):
                    self.dir = 2
                elif (us.rect.left>self.rect.right):
                    self.dir = 3
                else:
                    pass

                
                if self.dir == 3 and abs(self.rect.centerx - us.rect.centerx) < self.disAttack:
                    self.xspeed = global_speed_x + self.speed
                elif self.dir == 2 and abs(self.rect.centerx - us.rect.centerx) < self.disAttack :
                    self.xspeed = global_speed_x - self.speed
                else:
                    self.anim = "Walk"
                    

                
                self.rect.x += self.xspeed

                # if self.contidle < self.timeidle:
                #     self.contidle+=1
                #     self.anim = "idle"
                # elif self.contrun < self.timerun:
                #     self.contrun+=1
                #     self.anim = "Walk"
                # else:
                #     self.anim = "attack"


                for player in c.Grupos["player"]:
                    collide = pg.sprite.collide_circle(self,player)
                    if collide:
                        self.anim = "attack"
                        

                if self.n < self.speedanim:
                    self.n+=1
                else:
                    self.n = 0
                    if self.anim == "idle":
                        self.animlimit = 6
                    elif self.anim == "attack":
                        self.animlimit = 3
                    else:
                        self.animlimit = 1

                    if self.indexanim < self.animlimit:
                        self.indexanim+=1
                    else:
                        self.indexanim = 0
                #     # else:
                #     #     if self.anim == "attack":
                #     #         self.contrun = 0
                #     #         self.contidle = 0
                #     #     self.indexanim = 0
                
                try:
                    self.image = self.matrixAnimation[self.anim][0][self.indexanim]
                except IndexError:
                    self.indexanim = 0
                    self.image = self.matrixAnimation[self.anim][0][self.indexanim]



                # collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

                # for w in collision:
                #     if self.dir == 0:
                #         self.dir = 1
                #         self.rect.y-=5
                #     elif self.dir == 1:
                #         self.dir = 0
                #         self.rect.y+=5
                #     elif self.dir == 2:
                #         self.dir = 3
                #         self.rect.x+=5
                #     elif self.dir == 3:
                #         self.dir = 2
                #         self.rect.x -=5
                        
                # collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shoots"], False)

                # for b in collisionShoot:

                #     im = Impacto(self.rect.center, 0)
                #     c.Grupos["todos"].add(im)
                #     b.kill()
                #     self.vida-=b.damage
                if self.vida <= 0:
                    self.prueba+=1
                    self.live = False
                    self.kill()





class pato(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.name = "Pato"
        self.matrixAnimation = c.Enemy
        self.image = self.matrixAnimation["idle"][0][0]
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        # self.xspeed = 0
        # self.yspeed = 0
        self.speed = 2
        self.damage = 10
        self.live = True
        self.radius = 50
        self.cdano = 5


        self.disAttack = 100

        self.prueba  = 0

        self.playerDamageBow = 7

        self.vida = 40

        self.anim = "idle"
        self.dir = 0
        self.animlimit = 0

        # self.timeidle = random.randrange(100,200)
        # self.timerun = random.randrange(100,200)

        self.contrun = 0
        self.contidle = 0
        

        self.n = 0
        self.speedanim = 10
        self.indexanim = 0

        self.attackTick = 0
        self.damage = True

    def reward(self):
        n = random.randrange(3,6)
        for i in range(1):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), "Exp")
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)
        for i in range(n):
            x = random.randrange(-60,60)
            y = random.randrange(-60,60)
            t = random.randrange(0,3)
            if t == 0:
                tipo = "Exp"
            elif t == 1:
                tipo = "Resistencia"
            elif t ==  2:
                tipo = "Vida"

            cons = consumibleExp((self.rect.centerx + x - global_posicion_x, self.rect.centery + y - global_posicion_y), tipo)
            c.Grupos["consumibles"].add(cons)
            c.Grupos["todos"].add(cons)

    def disparar(self):
        b = Shoot(self.rect.center, 0, self.playerDamageBow, 0)
        c.Grupos["shootsEnemies"].add(b)
        c.Grupos["todos"].add(b)
    

    def update(self):
        self.xspeed = global_speed_x
        self.yspeed = global_speed_y

        if self.live:
            # if self.anim == "Walk":
            #     self.speed = 2
            # elif self.anim == "attack":
            #     self.speed = 2
            # else: 
            #     self.speed = 0
            
            for us in c.Grupos["player"]:

                # if (us.rect.right<self.rect.left):
                #     self.dir = 2
                # elif (us.rect.left>self.rect.right):
                #     self.dir = 3
                # else:
                #     pass

                
                # if self.dir == 3 and abs(self.rect.centerx - us.rect.centerx) < self.disAttack:
                #     self.xspeed = global_speed_x + self.speed
                # elif self.dir == 2 and abs(self.rect.centerx - us.rect.centerx) < self.disAttack :
                #     self.xspeed = global_speed_x - self.speed
                # else:
                #     self.anim = "Walk"
                    

                self.xspeed = global_speed_x + 0
                self.xspeed = global_speed_x - 0
                self.yspeed = global_speed_y + 0
                self.yspeed = global_speed_y - 0
                self.rect.x += self.xspeed
                self.rect.y += self.yspeed


                # if self.contidle < self.timeidle:
                #     self.contidle+=1
                #     self.anim = "idle"
                # elif self.contrun < self.timerun:
                #     self.contrun+=1
                #     self.anim = "Walk"
                # else:
                #     self.anim = "attack"


                for player in c.Grupos["player"]:
                    collide = pg.sprite.collide_circle_ratio(2.4)(self,player)
                    if collide:
                        self.anim = "attack"
                    else:
                        self.anim = "idle"
                        
                        

                if self.n < self.speedanim:
                    self.n+=1
                else:
                    self.n = 0
                    if self.anim == "idle":
                        self.animlimit = 0
                    elif self.anim == "attack":
                        self.animlimit = 5
                    # else:
                    #     self.animlimit = 1

                    if self.indexanim < self.animlimit:
                        self.indexanim+=1
                    else:
                        if self.anim == "attack":
                            self.disparar()
                            self.indexanim = 0

                    
                #     # else:
                #     #     if self.anim == "attack":
                #     #         self.contrun = 0
                #     #         self.contidle = 0
                #     #     self.indexanim = 0
                
                try:
                    self.image = self.matrixAnimation[self.anim][0][self.indexanim]
                except IndexError:
                    self.indexanim = 0
                    self.image = self.matrixAnimation[self.anim][0][self.indexanim]



                # collision = pg.sprite.spritecollide(self, c.Grupos["muros"], False)

                # for w in collision:
                #     if self.dir == 0:
                #         self.dir = 1
                #         self.rect.y-=5
                #     elif self.dir == 1:
                #         self.dir = 0
                #         self.rect.y+=5
                #     elif self.dir == 2:
                #         self.dir = 3
                #         self.rect.x+=5
                #     elif self.dir == 3:
                #         self.dir = 2
                #         self.rect.x -=5
                        
                collisionShoot = pg.sprite.spritecollide(self, c.Grupos["shoots"], False)

                for b in collisionShoot:

                    im = Impacto(self.rect.center, 0)
                    c.Grupos["todos"].add(im)
                    b.kill()
                    self.vida-=b.damage
                if self.vida <= 0:
                    self.prueba+=1
                    self.live = False
                    self.reward()
                    self.kill()

class CollisionChecker(pg.sprite.Sprite):
    def __init__(self, possize):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.Surface(((possize[2]),possize[3]))
        self.rect = self.image.get_rect()
        self.name = "default"
        self.rect.x = possize[0]
        self.rect.y = possize[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y
        self.layer = 2
    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y
        

class Impacto(pg.sprite.Sprite):
        def __init__(self, pos,tipo):
            pg.sprite.Sprite.__init__(self)
            self.tipo = tipo
            self.frames = 0
            if self.tipo == 0:
                self.MatrizAnimations = c.ItemSheets["Impacto"]
                self.frames = 8
            elif self.tipo == 1:
                self.MatrizAnimations = c.ItemSheets["Impacto1"]
                self.frames = 5
            elif self.tipo == 2:
                self.MatrizAnimations = c.ItemSheets["Impactogrande"]
                self.frames = 5
            self.image = self.MatrizAnimations[0][0]
            #self.image.fill(c.ROJO)
            self.rect = self.image.get_rect()
            self.rect.centerx = pos[0]
            self.rect.centery = pos[1]
            self.init_x = self.rect.x
            self.init_y = self.rect.y
            self.layer = 1
            self.radius = 20

            self.indexanim = 0
            self.n =0
            self.speedanim = 5

        def update(self):

            self.image = self.MatrizAnimations[0][self.indexanim]

            if self.n < self.speedanim:
                self.n += 1
            else:
                if self.indexanim < self.frames:
                    self.indexanim += 1
                else:
                    self.kill()

class indicator(pg.sprite.Sprite):
        def __init__(self, pos,texto,color):
            pg.sprite.Sprite.__init__(self)
            self.fuente = pg.font.Font(None, 20)
            self.text = texto
            self.color = color
            self.texto = self.fuente.render(self.text,True,self.color)
            self.image = self.texto
            self.rect = self.image.get_rect()
            self.rect.x = pos[0]
            self.rect.y = pos[1]
            self.init_x = self.rect.x
            self.init_y = self.rect.y
            self.speed = random.randrange(-2,2)
            self.speedyRandom = random.randrange(-2,2)
            self.speedxRandom = random.randrange(-2,2)
            self.speedx = 0
            self.speedy = 0

            self.timeSHow = 20

        def update(self):
            self.speedx = global_speed_x + self.speedxRandom
            self.speedy = global_speed_y + self.speedyRandom

            self.rect.x += self.speedx
            self.rect.y += self.speedy

            if self.timeSHow > 0:
                self.timeSHow -= 1
            else:
                #print "Bye"
                self.kill()

class Background(pg.sprite.Sprite):
    def __init__(self,tm,img):
        pg.sprite.Sprite.__init__(self)
        self.image = pg.transform.scale(img, tm)
        self.rect = self.image.get_rect()
        self.rect.x = 0
        self.rect.y = 0
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class consumibleExp(pg.sprite.Sprite):
    def __init__(self, pos, tipo):
        pg.sprite.Sprite.__init__(self)
        self.tipo = tipo
        if self.tipo == "Exp":
            self.matrixAnimation = c.ItemSheets["consumible"]
        if self.tipo == "Resistencia":
            self.matrixAnimation = c.ItemSheets["consumibleres"]
        if self.tipo == "Vida":
            self.matrixAnimation = c.ItemSheets["consumiblevida"]
        self.image = self.matrixAnimation[0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class consumibleMana(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.ItemSheets["consumibleres"]
        self.image = self.matrixAnimation[0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class consumibleVida(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.ItemSheets["consumible"]
        self.image = self.matrixAnimation[0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y
        

class gema(pg.sprite.Sprite):
    def __init__(self, pos):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.ItemSheets["gema"]
        self.image = self.matrixAnimation[0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = pos[0]
        self.rect.y = pos[1]
        self.init_x = self.rect.x
        self.init_y = self.rect.y

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

class Max(pg.sprite.Sprite):
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.pNJugables
        self.image = self.matrixAnimation["Walk"][0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = 300
        self.rect.y = 80
        self.init_x = self.rect.x
        self.init_y = self.rect.y
        self.n = 0
        self.speedAnim = 8
        self.indexanim = 0
        self.limit = 3

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

        if self.n == self.speedAnim:
            self.n = 0
            self.indexanim+=1
        else:
            self.n+=1

        if self.indexanim < self.limit:
            self.image = self.matrixAnimation["Walk"][0][self.indexanim]
        else:
            self.indexanim = 0
            self.image = self.matrixAnimation["Walk"][0][self.indexanim]

class Aaron(pg.sprite.Sprite):
    def __init__(self):
        pg.sprite.Sprite.__init__(self)
        self.matrixAnimation = c.pNJugables
        self.image = self.matrixAnimation["WalkMago"][0][0]
        # self.image.fill((0,0,255))
        self.rect = self.image.get_rect()
        self.rect.x = 176
        self.rect.y = 262
        self.init_x = self.rect.x
        self.init_y = self.rect.y
        self.n = 0
        self.speedAnim = 8
        self.indexanim = 0
        self.limit = 3

    def update(self):
        self.rect.x = self.init_x + global_posicion_x
        self.rect.y = self.init_y + global_posicion_y

        if self.n == self.speedAnim:
            self.n = 0
            self.indexanim+=1
        else:
            self.n+=1

        if self.indexanim < self.limit:
            self.image = self.matrixAnimation["WalkMago"][0][self.indexanim]
        else:
            self.indexanim = 0
            self.image = self.matrixAnimation["WalkMago"][0][self.indexanim]

        


