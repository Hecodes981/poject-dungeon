import Components as comp
import Constants as c
import pygame as pg
import random
import json


class State():
    def __init__(self,caption, estado_siguiente):
        self.caption = caption
        self.fin = False
        self.estado_siguiente = estado_siguiente
        self.ventana = pg.display.get_surface()

    def update(self):
        pass
    def setup(self):
        pg.display.set_caption(self.caption)
    def reboot(self):
        pass
    def boot(self):
        pass
    def __str__(self):
        return "Caption: %s, Estado Siguiente: %s" % (self.caption,self.estado_siguiente)
    def quit(self):
        self.fin = True
        self.makequit()
    def makequit(self):
        pass


#-----------------Clase para las pantallas de carga e inicio--------------#
class PantallaInicio(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.x = -500
        self.y = 32
        self.avance = 100
        self.ntran = 2
        self.tran = 1
        self.fuente = pg.font.Font(None,22)
        self.text = self.fuente.render("Presione A para ir al menu principal", True, c.NEGRO)

    def update(self):
        key = pg.key.get_pressed()
        if key[pg.K_a]:
            self.quit()
        self.ventana.fill(c.BLANCO)
        if self.tran == 1:
            self.ventana.blit(c.graficosPantallaPrincipal["LogoUTP"], (self.x, self.y))
        elif self.tran == 2:
            self.ventana.blit(c.graficosPantallaPrincipal["LogoISC"], (self.x,self.y))

        self.ventana.blit(self.text, (c.window_size[0] - 270, c.window_size[1]- 32))

        self.x+=self.avance

        if self.x < 99:
            self.avance/=1.2
        else:
            self.avance*=1.2

        if self.x > c.window_size[0]:
            if self.tran < self.ntran:
                self.tran +=1
            else:
                self.quit()
            self.x = -500
            self.avance = 100

class MenuPrincipal(State):
    def __init__(self,caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.fuente = pg.font.Font(None,22)
        self.text = self.fuente.render("Menu Prueba", True, c.NEGRO)
        self.punteroValue = 0
        self.validateButton = 0
        self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")

    def sound(self):

        self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")
        self.soundbgMenu.set_volume(0.07)
        self.soundbgMenu.play()

    def boot(self):
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/sword-art-online-original-soundtrack-vol-1-30-with-my-friend-[AudioTrimmer.com].ogg")
        self.soundbg.set_volume(0.7)
        self.soundbg.play()

    def makequit(self):
        self.soundbg.stop()
        # pg.mixer.quit()

    def update(self):

        self.ventana.fill((0,0,255))
        self.ventana.blit(c.GraficosMenu["backgroundMenu"], (0,0))
        self.ventana.blit(c.GraficosMenu["Title"], (200,100))        
        self.ventana.blit(c.GraficosMenu["StartMenu"], (398,300))
        self.ventana.blit(c.GraficosMenu["TutorialMenu"], (398,364))
        self.ventana.blit(c.GraficosMenu["CreditsMenu"], (398,428))
        self.ventana.blit(c.GraficosMenu["QuitMenu"], (398,492))
        self.ventana.blit(self.text, (c.window_size[0] - 300, c.window_size[1]- 32))

        if self.punteroValue == 0:
            self.ventana.blit(c.GraficosMenu["puntero"], (350,302))
        if self.punteroValue == 1:
            self.ventana.blit(c.GraficosMenu["puntero"], (350,366))
        if self.punteroValue == 2:
            self.ventana.blit(c.GraficosMenu["puntero"], (350,430))
        if self.punteroValue == 3:
            self.ventana.blit(c.GraficosMenu["puntero"], (350,494))            
         
        key = pg.key.get_pressed()

        if key[pg.K_DOWN] or key[pg.K_RIGHT]:
            self.sound()
            self.validateButton += 1
            if self.validateButton == 1:
                if self.punteroValue < 3:
                    self.punteroValue+=1
                else:
                    self.punteroValue = 0

        if key[pg.K_UP] or key[pg.K_LEFT]:
            self.sound()
            self.validateButton += 1
            if self.validateButton == 1:
                if self.punteroValue > 0:
                    self.punteroValue-=1
                else:
                    self.punteroValue = 2

        if not key[pg.K_DOWN] and not key[pg.K_LEFT] and not key[pg.K_RIGHT] and not key[pg.K_UP]:
            self.validateButton = 0
            self.soundbgMenu.stop()

        #----- aun faltan estados ----- Por ahora solo avanza sin importar la  opcion#
        if key[pg.K_SPACE]:
            if self.punteroValue == 0:
                self.estado_siguiente = "Level1Tutorial"
                self.quit()
            if self.punteroValue == 1:
                self.estado_siguiente = "tutorial"
                self.quit()
            if self.punteroValue == 2:
                self.estado_siguiente = "credits"
                self.quit()
            if self.punteroValue == 3:
                self.estado_siguiente = "QUIT"
                self.quit()



class GameOver(State):
    def __init__(self,caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.punteroValue = 0
        self.validateButton = 0
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")

    def sound(self):
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")
        self.soundbgMenu.set_volume(0.3)
        self.soundbgMenu.play()

    def boot(self):
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/Gameover2.ogg")
        self.soundbg.set_volume(0.7)
        self.soundbg.play()

    def makequit(self):
        self.soundbg.stop()
        pg.mixer.quit()

    def update(self):

        self.ventana.fill((0,0,255))
        self.ventana.blit(c.GameOver["Over"], (0,0))
        # self.ventana.blit(self.text, (c.window_size[0] - 300, c.window_size[1]- 32))          
         
        key = pg.key.get_pressed()

        #----- aun faltan estados ----- Por ahora solo avanza sin importar la  opcion#
        if key[pg.K_SPACE]:
            self.quit()

class TutorialTeclas(State):
    def __init__(self,caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.punteroValue = 0
        self.validateButton = 0
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")

    def sound(self):
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")
        self.soundbgMenu.set_volume(0.3)
        self.soundbgMenu.play()

    def boot(self):
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/Gameover2.ogg")
        self.soundbg.set_volume(0.7)
        self.soundbg.play()

    def makequit(self):
        self.soundbg.stop()
        pg.mixer.quit()

    def update(self):

        self.ventana.fill((0,0,255))
        self.ventana.blit(c.GameOver["Over"], (0,0))
        # self.ventana.blit(self.text, (c.window_size[0] - 300, c.window_size[1]- 32))          
         
        key = pg.key.get_pressed()

        #----- aun faltan estados ----- Por ahora solo avanza sin importar la  opcion#
        if key[pg.K_SPACE]:
            self.quit()

#------PRIEBA PROLOGO ----#

class prologoPrueba(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.fuente = pg.font.Font(None, 28)
        self.fuentePass = pg.font.Font(None, 22)
        self.espacio =  13
        self.pos_x = 200
        self.prueba = 'En este punto, Alex ya sabe como defenderse en'
        self.text2 = 'el mundo de Tesalia, y esta listo para combatir '
        self.text3 =  'Cualquier enemigo    Pero . . .'
        self.Pass = 'Presione B para continuar'
        self.Y = 0
        self.size = len(self.prueba)
        self.size2 = len(self.text2)
        self.size3 = len(self.text3)
        self.i = 0
        self.j = 0
        self.k = 0
        self.tick = 0
        self.imageEntry = False
        self.validateText = 0
        self.text = self.fuente.render(self.prueba[self.i], True, c.BLANCO)
        self.textPass = self.fuentePass.render(self.Pass, True, c.BLANCO)

    def reboot(self):
        c.limpiarGrupos()


    def boot(self):
        c.limpiarGrupos()
        # self.soundbg = pg.mixer.Sound("Recursos/Sonidos/sound1.wav")
        # self.soundbg.set_volume(0.7)
        # self.soundbg.play()
        self.makeboot()

    def makeboot(self):
        self.ventana.fill((0,0,0))
        self.ventana.blit(c.GraficosInterludios["backgroundInterludio1"], (0,-300))

    def makequit(self):
        pass
        # self.soundbg.stop()
        # pg.mixer.quit()


    def update(self):
        if self.tick == 7:
            self.pos_x += self.espacio
            if self.i < self.size and self.validateText == 0:
                self.text = self.fuente.render(self.prueba[self.i], True, c.BLANCO)
                self.i+=1
                self.ventana.blit(self.text, (self.pos_x, 440 + self.Y))
            
            if self.i == self.size:
                self.i = self.i + 100
                self.validateText =  1
                self.Y = 40
                
                
            if self.validateText == 1 and self.j == 0:
                self.pos_x = 200

            if self.j < self.size2 and self.validateText == 1:
                self.text = self.fuente.render(self.text2[self.j], True, c.BLANCO)
                self.j +=1
                self.ventana.blit(self.text, (self.pos_x + 20, 440 + self.Y))

            if self.j == self.size2 - 1:
                self.validateText = 2
                self.Y = 80

            if self.validateText == 2 and self.k == 0:
                self.pos_x = 200
            
            if self.k < self.size3 and self.validateText == 2:
                self.text = self.fuente.render(self.text3[self.k], True, c.BLANCO)
                self.k +=1
                self.ventana.blit(self.text, (self.pos_x + 20, 440 + self.Y))
            
            if self.validateText == 2:
                self.ventana.blit(self.textPass, (c.window_size[0] - 200, c.window_size[1] - 20))

            # if self.validateText >= 1:
            #    self.ventana.blit(c.GraficosMenu["Title"], (200,100))        

        
            self.tick = 0
        self.tick+=1

        key = pg.key.get_pressed()
        if key[pg.K_b]:
            self.quit()

class history(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        # self.fuente = pg.font.Font(None, 23)
        self.fuente = pg.font.SysFont('t4cbeaulieux', 20)
        self.fuentePass = pg.font.Font(None, 22)
        self.espacio =  13
        self.pos_x = 200
        self.prueba = 'En una noche de estrellas muriendo un chico comun y corriente observa'
        self.text2 = 'los cometas que no parecen ser los de siempre'
        
        self.Pass = 'Presione B para continuar'
        self.Y = 0
        self.xm = c.window_size[0] + 10
        self.transition = 0
        self.tick = 0
        self.imageEntry = False
        self.validateText = 0
        self.text = self.fuente.render(self.prueba, True, c.BLANCO)
        self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
        self.validateTransition = True

    def reboot(self):
        c.limpiarGrupos()


    def boot(self):
        c.limpiarGrupos()
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/fairy.ogg")
        self.soundbg.set_volume(0.7)
        self.soundbg.play()
        self.makeboot()

    def makeboot(self):
        self.ventana.fill((0,0,0))

    def makequit(self):
        self.soundbg.stop()
        # pg.mixer.quit()
        # self.soundbg.stop()
        # pg.mixer.quit()


    def update(self):

        self.ventana.fill((0,0,0))
        if self.transition == 0:
            self.ventana.blit(c.GraficosInterludios["historia1"], (self.xm, 0))
            if self.xm > 0:
                self.xm -=15
            else:
                self.xm = 0
                self.transition = 1
        if self.transition == 1:
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(self.text, (150, c.window_size[1] - 90))
            self.ventana.blit(self.text2, (150, c.window_size[1] - 60))
            self.xm = c.window_size[0] + 10        
        if self.transition == 2:
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (self.xm, 0))
            if self.xm > 322:
                self.xm -=15
            else:
                self.xm = 0
                self.transition = 3
        if self.transition == 3:
            self.prueba = 'De repente es golpeado por los cometas. Su fuerza magica lo transporto'
            self.text2 = 'otro mundo dandole asi la habilidad de viajar entre ellos'
            self.text = self.fuente.render(self.prueba, True, c.BLANCO)
            self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (322, 0))
            self.ventana.blit(self.text, (150, c.window_size[1] - 90))
            self.ventana.blit(self.text2, (150, c.window_size[1] - 60))
            self.xm = c.window_size[0] + 10        
        if self.transition == 4:
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (322, 0))
            self.ventana.blit(c.GraficosInterludios["historia3"], (self.xm, 0))
            if self.xm > 644:
                self.xm -=15
            else:
                self.xm = 0
                self.transition = 5
        if self.transition == 5:
            self.prueba = 'Desesperado viaja entre mundos buscando la forma de salir hasta que cansado cae'
            self.text2 = 'en un mundo de fantasia donde encuentra una luz de esperanza'
            self.text = self.fuente.render(self.prueba, True, c.BLANCO)
            self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (322, 0))
            self.ventana.blit(c.GraficosInterludios["historia3"], (644, 0))
            self.ventana.blit(self.text, (150, c.window_size[1] - 90))
            self.ventana.blit(self.text2, (150, c.window_size[1] - 60))
            self.xm = c.window_size[0] + 10        

        if self.transition == 6:
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (322, 0))
            self.ventana.blit(c.GraficosInterludios["historia3"], (644, 0))
            self.ventana.blit(c.GraficosInterludios["book"], (self.xm, 100))
            if self.xm > 240:
                self.xm -=15
            else:
                self.xm = 0
                self.transition = 7
        if self.transition == 7:
            self.prueba = 'El diario de un atiguo viajero de mundos, relatando una forma de volver a casa'
            self.text2 = 'usando el poder de una gema, para conseguirla tendrá que atravesar toda Tesalia'
            self.text3 = 'sin importar que obstaculo aparezca'
            self.text = self.fuente.render(self.prueba, True, c.BLANCO)
            self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
            self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
            self.ventana.blit(c.GraficosInterludios["historia1"], (0, 0))
            self.ventana.blit(c.GraficosInterludios["historia2"], (322, 0))
            self.ventana.blit(c.GraficosInterludios["historia3"], (644, 0))
            self.ventana.blit(c.GraficosInterludios["book"], (240, 100))
            self.ventana.blit(self.text, (150, c.window_size[1] - 120))
            self.ventana.blit(self.text2, (150, c.window_size[1] - 90))
            self.ventana.blit(self.text3, (150, c.window_size[1] - 60))
            self.xm = c.window_size[0] + 10
        if self.transition == 8:
            self.ventana.fill((0,0,0))
            self.prueba = 'Aqui empieza la aventura de Alex . . .'
            
            self.text = self.fuente.render(self.prueba, True, c.BLANCO)
            self.ventana.blit(self.text, (60, c.window_size[1] - 120))
            self.xm = c.window_size[0] + 10


        # print(pg.font.get_fonts())
        key = pg.key.get_pressed()
        if key[pg.K_b]:
            if self.transition == 8:
                self.quit()
        if key[pg.K_SPACE]:
            if self.validateTransition:
                if self.transition < 8:
                    self.transition +=1
                    self.validateTransition = False

        if not key[pg.K_SPACE]:
            self.validateTransition = True



class Interludio1(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.fuente = pg.font.SysFont('constantia', 20)
        # self.fuente = pg.font.Font(None, 26)
        self.fuentePass = pg.font.Font(None, 22)
        self.espacio =  13
        self.pos_x = 200
        self.prueba = 'Hola Alex!, te puedo llamar asi?, te he estado siguiendo y'
        self.text2 = 'veo que buscas las forma de volvera tu mundo '
        self.text3 =  '. . .'
        self.Pass = 'Presione A para continuar'
        self.Y = 0

        self.xm = -350
        self.xA = c.window_size[0] + 110
        self.powm = 90
        self.powa = 90

        self.validate = False
        self.apuntadorPos = 110
        self.posAp = 0

        self.size = len(self.prueba)
        self.size2 = len(self.text2)
        self.size3 = len(self.text3)
        self.i = 0
        self.j = 0
        self.k = 0
        self.tick = 0
        self.imageEntry = False
        self.validateText = 0
        self.textValue = 0
        self.text = self.fuente.render(self.prueba, True, c.BLANCO)
        self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
        self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
        self.textPass = self.fuentePass.render(self.Pass, True, c.BLANCO)

    def reboot(self):
        c.limpiarGrupos()


    def boot(self):
        c.limpiarGrupos()
        # self.soundbg = pg.mixer.Sound("Recursos/Sonidos/sound1.wav")
        # self.soundbg.set_volume(0.7)
        # self.soundbg.play()
        self.makeboot()

    def makeboot(self):
        self.ventana.fill((0,0,0))
        print(pg.font.get_fonts())

    def makequit(self):
        pass
        # self.soundbg.stop()
        # pg.mixer.quit()


    def update(self):
    
        self.ventana.fill((0,0,0))
        self.ventana.blit(c.GraficosInterludios["backgroundInterludio2"], (0,0))
        self.ventana.blit(c.GraficosInterludios["dialogo"].convert_alpha(), (90, 385))
        self.ventana.blit(c.GraficosInterludios["dialogoA"].convert_alpha(), (90, 5))
        self.ventana.blit(c.GraficosInterludios["mago"], (self.xm, 410))
        self.ventana.blit(c.GraficosInterludios["alex"], (self.xA, 60))        
        # if self.validateText >= 1:
        #    self.ventana.blit(c.GraficosMenu["Title"], (200,100))        


        self.tick = 0
        self.xm +=self.powm

        if self.xm < 630:
            self.powm/=1.1
        else:
            self.powm = 0

        self.xA -=self.powa

        if self.xA > 110:
            self.powa/=1.1
        else:
            self.powa = 0

        if self.powa == 0 and self.powm == 0:
            if self.textValue == 0:
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
                self.ventana.blit(self.text3, (270, 140))
            if self.textValue == 1:
                self.prueba = 'Yo te puedo ayudar, conozco la forma de salir, pero para'
                self.text2 = 'ello necesito de tu poder... '
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
            if self.textValue == 2:
                self.text3 =  'Quien eres tu, y como sabes eso?'
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.ventana.blit(self.text3, (270, 140))
            if self.textValue == 3:
                self.prueba = 'Me llamo Aaron, era un viajero de dimensiones'
                self.text2 = 'al igual que tu, pero perdi mi poder '
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
            if self.textValue == 4:
                self.prueba = 'Si me ayudas, ambos saldremos, que dices'
                self.text2 = 'Aceptas?!?'
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
            if self.textValue == 5:
                self.text3 =  '(... No lo sé, es muy bueno para ser cierto!)'
                self.text4 =  '(... Pero si lo es, porfin podria salir de aqui!)'
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.text4 = self.fuente.render(self.text4, True, c.BLANCO)
                self.ventana.blit(self.text3, (270, 100))
                self.ventana.blit(self.text4, (270, 140))
            

            if self.textValue == 6:
                self.text3 =  'Digo que... :'
                self.text4 =  'Si'
                self.text5 =  'No'
                self.text6 = ">>>"
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.text4 = self.fuente.render(self.text4, True, c.BLANCO)
                self.text5 = self.fuente.render(self.text5, True, c.BLANCO)
                self.text6 = self.fuente.render(self.text6, True, c.BLANCO)
                self.ventana.blit(self.text3, (270, 80))
                self.ventana.blit(self.text4, (300, 110))
                self.ventana.blit(self.text5, (300, 140))
                self.ventana.blit(self.text6, (250, self.apuntadorPos))
            if self.textValue == 7:
                if self.posAp == 0:
                    self.text3 =  'Acepto, que debo hacer?'
                    self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                    self.ventana.blit(self.text3, (300, 80))
                if self.posAp == 1:
                    self.text3 =  'No gracias, no confio en forasteros.'
                    self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                    self.ventana.blit(self.text3, (300, 80))
                self.text4 =  'Presiona A para avanzar'
                self.text4 = self.fuente.render(self.text4, True, c.BLANCO)
                self.ventana.blit(self.text4, (400, 310))


                    


        key = pg.key.get_pressed()
        if key[pg.K_a]:
            if self.posAp == 0:
                self.estado_siguiente = "LevelPlatform"
                self.quit()
            elif self.posAp == 1:
                self.estado_siguiente = "Level3"
                self.quit()

        if key[pg.K_SPACE]:
            if self.validate:
                self.textValue+=1
                self.validate = False
        if not key[pg.K_SPACE]:
            self.validate = True

        if key[pg.K_DOWN] and self.textValue == 6:
            if self.posAp == 0:
                self.apuntadorPos = 140
                self.posAp = 1
            elif self.posAp == 1:
                self.apuntadorPos = 110
                self.posAp = 0


class Interludio2(State):
    def __init__(self, caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.fuente = pg.font.SysFont('constantia', 20)
        # self.fuente = pg.font.Font(None, 26)
        self.fuentePass = pg.font.Font(None, 22)
        self.espacio =  13
        self.pos_x = 200
        self.prueba = 'Oye...!'
        self.text2 = 'Estas bien?'
        self.text3 =  '*Aghh* Donde estoy?'
        self.Pass = 'Presione A para continuar'
        self.Y = 0

        self.xm = -350
        self.xA = c.window_size[0] + 110
        self.powm = 90
        self.powa = 90

        self.validate = False
        self.apuntadorPos = 110
        self.posAp = 0

        self.size = len(self.prueba)
        self.size2 = len(self.text2)
        self.size3 = len(self.text3)
        self.i = 0
        self.j = 0
        self.k = 0
        self.tick = 0
        self.imageEntry = False
        self.validateText = 0
        self.textValue = 0
        self.text = self.fuente.render(self.prueba, True, c.BLANCO)
        self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
        self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
        self.textPass = self.fuentePass.render(self.Pass, True, c.BLANCO)

    def reboot(self):
        c.limpiarGrupos()


    def boot(self):
        c.limpiarGrupos()
        # self.soundbg = pg.mixer.Sound("Recursos/Sonidos/sound1.wav")
        # self.soundbg.set_volume(0.7)
        # self.soundbg.play()
        self.makeboot()

    def makeboot(self):
        self.ventana.fill((0,0,0))
        print(pg.font.get_fonts())

    def makequit(self):
        pass
        # self.soundbg.stop()
        # pg.mixer.quit()


    def update(self):
    
        self.ventana.fill((0,0,0))
        self.ventana.blit(c.GraficosInterludios["backgroundInterludio3"], (0,0))
        self.ventana.blit(c.GraficosInterludios["dialogo"].convert_alpha(), (90, 385))
        self.ventana.blit(c.GraficosInterludios["dialogoA"].convert_alpha(), (90, 5))
        self.ventana.blit(c.GraficosInterludios["Lily"], (self.xm, 435))
        self.ventana.blit(c.GraficosInterludios["alex"], (self.xA, 60))        
        # if self.validateText >= 1:
        #    self.ventana.blit(c.GraficosMenu["Title"], (200,100))        


        self.tick = 0
        self.xm +=self.powm
        if self.xm < 630:
            self.powm/=1.1
        else:
            self.powm = 0

        self.xA -=self.powa

        if self.xA > 110:
            self.powa/=1.1
        else:
            self.powa = 0

        if self.powa == 0 and self.powm == 0:
            if self.textValue == 0:
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
                self.ventana.blit(self.text3, (270, 140))
            if self.textValue == 1:
                self.prueba = 'Estas en la ruinas de un antiguo reino de Tesalia'
                self.text2 = 'Caiste desde muy alto, que te paso? '
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
            if self.textValue == 2:
                self.text3 =  'Alto... acantilado... gema...'
                self.text4 =  'Ya recuerdo, ese Aaron me tendio una trampa'
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.text4 = self.fuente.render(self.text4, True, c.BLANCO)
                self.ventana.blit(self.text3, (270, 100))
                self.ventana.blit(self.text4, (270, 140))
            if self.textValue == 3:
                self.prueba = 'Aaron?!?! ese es el nombre del mago que me robo '
                self.text2 = 'mis poderes asi que tampoco eres de aqui,  eh?'
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.ventana.blit(self.text, (140,490))
                self.ventana.blit(self.text2, (140,520))
            if self.textValue == 4:
                self.text3 = 'Es cierto, soy de otro mundo, busco la gema para salir de aqui'
                self.text4 = 'Esta custodiada por un minotauro'
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.text4 = self.fuente.render(self.text4, True, c.BLANCO)
                self.ventana.blit(self.text3, (270, 100))
                self.ventana.blit(self.text4, (270, 140))
            if self.textValue == 5:
                self.prueba = 'Ese minoatauro del que hablas es el rey'
                self.text2 = 'de estas ruinas para derrotarlo necesitas vencer a sus'
                self.text3 = 'subditos y encontrar el arco sagrado'
                self.text = self.fuente.render(self.prueba, True, c.BLANCO)
                self.text2 = self.fuente.render(self.text2, True, c.BLANCO)
                self.text3 = self.fuente.render(self.text3, True, c.BLANCO)
                self.ventana.blit(self.text, (140,470))
                self.ventana.blit(self.text2, (140,500))
                self.ventana.blit(self.text3, (140,520))
            


        key = pg.key.get_pressed()

        if key[pg.K_SPACE]:
            if self.validate:
                self.textValue+=1
                self.validate = False

            if self.textValue == 6:
                self.quit()

        if not key[pg.K_SPACE]:
            self.validate = True

        if key[pg.K_DOWN] and self.textValue == 6:
            if self.posAp == 0:
                self.apuntadorPos = 140
                self.posAp = 1
            elif self.posAp == 1:
                self.apuntadorPos = 110
                self.posAp = 0


class creditos(State):
    def __init__(self,caption, estado_siguiente):
        State.__init__(self, caption, estado_siguiente)
        self.surface = pg.Surface(c.window_size)
        self.punteroValue = 0
        self.validateButton = 0
        self.ym = c.window_size[1] + 200
        self.ym1 = self.ym + 909
        self.powm = 3
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")

    def sound(self):
        # self.soundbgMenu = pg.mixer.Sound("Recursos/Sonidos/change.ogg")
        self.soundbgMenu.set_volume(0.3)
        self.soundbgMenu.play()

    def boot(self):
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/ori-and-the-blind-forest-credits-pc.ogg")
        self.soundbg.set_volume(0.7)
        self.soundbg.play()

    def makequit(self):
        self.soundbg.stop()
        # pg.mixer.quit()

    def update(self):

        self.ventana.fill((0,0,255))
        self.ventana.blit(c.Credits["credits"], (0,0))
        self.ventana.blit(c.Credits["credits1"], (0, self.ym))
        self.ventana.blit(c.Credits["credits2"], (170, self.ym1))
        
        if self.ym > -1000:
            self.ym-=self.powm
        else:
            self.ym = -1000

        if self.ym1 > -2100:
            self.ym1-=self.powm
        else:
            self.ym1 = -2100
        
        # self.ventana.blit(self.text, (c.window_size[0] - 300, c.window_size[1]- 32))          
         
        key = pg.key.get_pressed()

        #----- aun faltan estados ----- Por ahora solo avanza sin importar la  opcion#
        if key[pg.K_SPACE]:
            self.quit()


        
class Level(State):
    def __init__(self, caption, estado_siguiente, world_size, titulo, mapeado, mus):
        State.__init__(self, caption, estado_siguiente)
        self.fuente = pg.font.Font(None, 16)
        self.fuentelvl = pg.font.Font(None, 24)
        self.timeWait = 150
        self.world_size = world_size
        self.completado =  False
        self.die =  False
        self.fuenteTitulo = pg.font.Font(None,72)
        self.mus = mus

        with open(mapeado) as archivo:
            self.datos = json.load(archivo)

        self.xm = 300
        self.ym = -800
        self.powm = 100


    def reboot(self):
        
        print("pase por aqui")
        c.limpiarGrupos()

    def boot(self):
        print("hola")
        pg.mixer.init()
        self.soundbg = pg.mixer.Sound("Recursos/Sonidos/"+self.mus)
        # self.soundbg.stop()
        self.soundbg.play(loops=-1)
        # self.timewait = 150
        # self.completado = False
        # comp.global_posicion_x = 0
        # comp.global_posicion_y = -335
        # self.x = -1000
        # self.y = 512
        # self.pow = 100
        # self.ftitle = True
        # self.xm = -1000
        # self.ym = 512
        # self.powm = 100
        self.makeboot()
        

    def makeboot(self):
        pass
     
    def makequit(self):
        self.soundbg.stop()
        # pg.mixer.quit()

    

    def buscarCapa(self, capa):
        index = -1
        f = False
        for i in self.datos["layers"]:
            index+=1
            if i["name"] == capa:
                f = True
                break
        if f:
            return index
        else:
            return -1

    def generarMuros(self):
        index = self.buscarCapa("Muros")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                possize = (i["x"], i["y"], i["width"], i["height"])
                m = comp.Muro(possize)
                c.Grupos["muros"].add(m)
                c.Grupos["todos"].add(m)
        else:
            print("no se generaron muros")
            

    def generarBow(self):
        index = self.buscarCapa("Bow")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Bow(pos)
                c.Grupos["bow"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron arcos")

    def generarSpear(self):
        index = self.buscarCapa("Spear")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Spear(pos)
                c.Grupos["spear"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron lanzas")
    
    def generargema(self):
        index = self.buscarCapa("gema")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.gema(pos)
                c.Grupos["gemas"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron gemas")

    def generarEnemigos(self):
        index = self.buscarCapa("Enemy")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Enemy(pos)
                c.Grupos["enemy"].add(b)
                c.Grupos["enemigos"].add(b)
                c.Grupos["enemies"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron enemigos")

    def generarFish(self):
        index = self.buscarCapa("Fish")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Fish(pos)
                c.Grupos["fish"].add(b)
                c.Grupos["enemigos"].add(b)
                c.Grupos["enemies"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron Fish")

    def generarFoxy(self):
        index = self.buscarCapa("Foxy")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Foxy(pos)
                c.Grupos["foxy"].add(b)
                c.Grupos["enemigos"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron foxy's")

    def generarPato(self):
        index = self.buscarCapa("pato")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.pato(pos)
                c.Grupos["pato"].add(b)
                c.Grupos["enemigos"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron patos")
    
    def generarBicho(self):
        index = self.buscarCapa("Bicho")
        if index > -1:
            for i in self.datos["layers"][index]["objects"]:
                pos = (i["x"], i["y"])
                b = comp.Bicho(pos)
                c.Grupos["bicho"].add(b)
                c.Grupos["enemigos"].add(b)
                c.Grupos["enemies"].add(b)
                c.Grupos["todos"].add(b)
        else:
            print("no se generaron bichos")


    def update2(self):
        pass

    def update(self):
        self.ventana.fill(c.NEGRO)
        c.Grupos["todos"].update()
        c.Grupos["todos"].draw(self.ventana)
        c.Grupos["fish"].draw(self.ventana)
        # c.Grupos["bow"].draw(self.ventana)
        # c.Grupos["spear"].draw(self.ventana)
        # c.Grupos["enemy"].draw(self.ventana)
        # c.Grupos["player"].draw(self.ventana)
        pg.draw.rect(self.ventana, (100,100,100), (95,51,self.player.vidaMax, 28))
        pg.draw.rect(self.ventana, (147, 20, 16), (95,51,self.player.vida, 28))
        pg.draw.rect(self.ventana, (100,100,100), (95,82,self.player.manaMax, 24))
        pg.draw.rect(self.ventana, (0,0,200), (95,82,self.player.mana, 24))
        self.ventana.blit(c.ItemSheets["Hud"],(16,16))
        nivelexp = self.fuente.render("Exp {}/{}, Salud {}".format(self.player.exp,self.player.expsiguientenivel,int(self.player.vida)),True,c.NEGRO)
        nivel = self.fuentelvl.render("{}".format(self.player.nivel),True,c.BLANCO)        
        self.ventana.blit(nivelexp,(150,60))
        self.ventana.blit(nivel,(64,121))

        
        if self.completado:
            text1 = self.fuenteTitulo.render("Bien hecho", True, c.NEGRO)
            self.ventana.blit(text1, (self.xm-3, self.ym+3))
            text = self.fuenteTitulo.render("Bien hecho", True, c.CELESTE)
            self.ventana.blit(text, (self.xm, self.ym))
            self.ym += self.powm
            if self.ym < 299:
                self.powm = self.powm/1.1
            else:
                self.powm = self.powm*1.1
            self.timeWait -= 1

        key = pg.key.get_pressed()

        if key[pg.K_2]:
            self.estado_siguiente = "LevelPlatform"
            self.quit()
        if key[pg.K_3]:
            self.estado_siguiente = "Level3"
            self.quit()

        if self.die:
            self.estado_siguiente = "GameOver"
            self.quit() 

        if not self.player.live:
            self.quit()

        if self.timeWait <= 0:
            if not self.player.live:
                c.playerHacks["Nivel"] = 1
                c.playerHacks["Exp"]  = 0
            else:
                c.playerHacks["Nivel"] = self.player.nivel
                c.playerHacks["Exp"]  = self.player.exp 
            self.quit()
        # if self.prueba:
        #     print("hola")

        self.update2()

class Level1Tutorial(Level):
    def __init__(self, caption, estado_siguiente):
        Level.__init__(self, caption, estado_siguiente, (1536, 1792), "Levelt 1 tutorial", "Recursos/Mapeo/MapeoLvl1.json", "Saolvl1.ogg")
        self.fuenteTime = pg.font.Font(None,50)
        self.fuentes = pg.font.Font(None,32)

    

    def makeboot(self):

        self.bg = comp.Background(self.world_size, c.LevelTutorial["Background"])
        self.bg.rect.x = 0
        self.bg.rect.y = 0
        self.player = comp.Player(self.world_size, False)
        self.player.rect.x = 340
        self.player.rect.y = 0
        # self.weapon = comp.Weapons()
        self.max = comp.Max()
        self.prueba = False
        self.min = 1
        self.seg = 59
        self.titulo = 'OBJETIVOS'
        self.texto = 'Hazte con todos los'
        self.texto2 = 'enemigos'
        self.titulo1 = 'OBJETIVOS'
        self.texto1 = 'Hazte con todos los'
        self.texto21 = 'enemigos'
        self.generarMuros()
        
        self.tick = 0

        c.Grupos["todos"].add(self.bg)
        self.generarBow()
        self.generarEnemigos()
        self.generarBicho()
        self.generarSpear()
        c.Grupos["player"].add(self.player)
        c.Grupos["todos"].add(self.max)
        c.Grupos["todos"].add(self.player)

    def update2(self):
        # c.Grupos["consumibles"].draw(self.ventana) 

        self.ventana.blit(c.LevelTutorial["Nube"], (self.max.rect.x - 70, self.max.rect.y - 70))

        if self.player.nivel == 3:
            self.completado = True

        if not self.player.vivito:
            self.die = True 
        
        if self.tick == 60:
            self.tick = 0
            self.seg-=1 
        else:
            self.tick+=1

        if self.seg == 0:
            self.seg = 59
            self.min = 0

        texto = self.fuentes.render(self.titulo, True, c.BLANCO)
        texto1 = self.fuentes.render(self.texto, True, c.BLANCO)
        texto2 = self.fuentes.render(self.texto2, True, c.BLANCO)
        texto3 = self.fuentes.render(self.titulo1, True, c.NEGRO)
        texto4 = self.fuentes.render(self.texto1, True, c.NEGRO)
        texto5 = self.fuentes.render(self.texto21, True, c.NEGRO)
        self.ventana.blit(c.ItemSheets["timer"].convert_alpha(), (390,-17))

        text = self.fuenteTime.render( "{} : {}".format(self.min, self.seg), True, (c.BLANCO) )
        self.ventana.blit(text, (450, 30))
        
        self.ventana.blit(texto3, (747, 73))
        self.ventana.blit(texto4, (747, 103))
        self.ventana.blit(texto5, (747, 123))
        self.ventana.blit(texto, (750, 70))
        self.ventana.blit(texto1, (750, 100))
        self.ventana.blit(texto2, (747, 120))

        
        # key = pg.key.get_pressed()
        # if key[pg.K_0]:
        #     self.prueba = True

class LevelPlatform(Level):
    def  __init__(self, caption, estado_siguiente):
        Level.__init__(self, caption, estado_siguiente, (2048, 604), "Level prefacio platform", "Recursos/Mapeo/platform.json", "taberna.ogg")
        self.fuentes = pg.font.Font(None,32)
    	
    def makeboot(self):

        self.titulo = 'OBJETIVOS'
        self.texto = 'Ayuda a Aaron'
        self.texto2 = 'buscando la gema'
        self.texto3 = 'al final'
            


        self.bg = comp.Background(self.world_size, c.LevelPrefacio["Background"])
        self.player = comp.Player(self.world_size, True)
        comp.global_posicion_x = 0
        comp.global_posicion_y = 0
        self.player.rect.x = 10
        self.player.rect.y = 10
        self.generarMuros()
        c.Grupos["todos"].add(self.bg)
        self.aron = comp.Aaron()
        self.generarFish()
        self.generargema()
        self.generarSpear()
        c.Grupos["todos"].add(self.aron)
        c.Grupos["player"].add(self.player)
        c.Grupos["todos"].add(self.player)


    def update2(self):
        texto = self.fuentes.render(self.titulo, True, c.BLANCO)
        texto1 = self.fuentes.render(self.texto, True, c.BLANCO)
        texto2 = self.fuentes.render(self.texto2, True, c.BLANCO)
        texto3 = self.fuentes.render(self.titulo, True, c.NEGRO)
        texto4 = self.fuentes.render(self.texto, True, c.NEGRO)
        texto5 = self.fuentes.render(self.texto2, True, c.NEGRO)

        self.ventana.blit(texto3, (747, 73))
        self.ventana.blit(texto4, (747, 103))
        self.ventana.blit(texto5, (747, 123))
        self.ventana.blit(texto, (750, 70))
        self.ventana.blit(texto1, (750, 100))
        self.ventana.blit(texto2, (747, 120))

class Level3(Level):
    def  __init__(self, caption, estado_siguiente):
        Level.__init__(self, caption, estado_siguiente, (1800, 1800), "Level 3", "Recursos/Mapeo/lvl3.json", "lvl3.ogg")
        
        self.fuenteTime = pg.font.SysFont('t4cbeaulieux', 40)
        self.fuentes = pg.font.Font(None,32)

    def makeboot(self):
        self.bg = comp.Background(self.world_size, c.Level3["Background"])
        self.player = comp.Player(self.world_size, False)
        comp.global_posicion_x = 0
        comp.global_posicion_y = 0
        self.player.rect.x = 100
        self.player.rect.y = 500
        self.tick = 0
        self.seg = 59
        self.min =  1

        self.titulo = 'OBJETIVOS'
        self.texto = 'busca el arco'
        self.texto2 = 'sagrado'
        

        self.generarMuros()
        c.Grupos["todos"].add(self.bg)
        # self.generarFish()
        self.generarEnemigos()
        self.generarBow()
        self.generarPato()
        self.generarFoxy()
        c.Grupos["player"].add(self.player)
        c.Grupos["todos"].add(self.player)

    def update2(self):
        if self.tick == 60:
            self.tick = 0
            self.seg-=1 
        else:
            self.tick+=1

        if self.seg == 0:
            self.seg = 59
            self.min = 0

        if self.player.bow:
            self.completado =  True

        texto = self.fuentes.render(self.titulo, True, c.BLANCO)
        texto1 = self.fuentes.render(self.texto, True, c.BLANCO)
        texto2 = self.fuentes.render(self.texto2, True, c.BLANCO)
        texto3 = self.fuentes.render(self.titulo, True, c.NEGRO)
        texto4 = self.fuentes.render(self.texto, True, c.NEGRO)
        texto5 = self.fuentes.render(self.texto2, True, c.NEGRO)
        self.ventana.blit(c.ItemSheets["timer"].convert_alpha(), (390,-17))

        text = self.fuenteTime.render( "{} : {}".format(self.min, self.seg), True, c.BLANCO )
        self.ventana.blit(text, (445, 20))
        
        self.ventana.blit(texto3, (747, 73))
        self.ventana.blit(texto4, (747, 103))
        self.ventana.blit(texto5, (747, 123))
        self.ventana.blit(texto, (750, 70))
        self.ventana.blit(texto1, (750, 100))
        self.ventana.blit(texto2, (747, 120))

class LevelFinal(Level):
    def  __init__(self, caption, estado_siguiente):
        Level.__init__(self, caption, estado_siguiente, (1800, 1800), "Level Final", "Recursos/Mapeo/Final.json", "Saolvl1.ogg")
        
        self.fuenteTime = pg.font.SysFont('t4cbeaulieux', 40)
        self.fuentes = pg.font.Font(None,32)

    	

    def makeboot(self):
        self.bg = comp.Background(self.world_size, c.LevelFinal["Background"])
        self.player = comp.Player(self.world_size, False)
        comp.global_posicion_x = 0
        comp.global_posicion_y = 0
        self.player.rect.x = 100
        self.player.rect.y = 500
        self.tick = 0
        self.seg = 59
        self.min =  1

        self.titulo = 'OBJETIVOS'
        self.texto = 'busca el arco'
        self.texto2 = 'sagrado'
        

        self.generarMuros()
        c.Grupos["todos"].add(self.bg)
        # self.generarFish()
        self.generarEnemigos()
        self.generarBow()
        self.generarPato()
        self.generarFoxy()
        c.Grupos["player"].add(self.player)
        c.Grupos["todos"].add(self.player)

    def update2(self):
        if self.tick == 60:
            self.tick = 0
            self.seg-=1 
        else:
            self.tick+=1

        if self.seg == 0:
            self.seg = 59
            self.min = 0

        if self.player.bow:
            self.completado =  True

        texto = self.fuentes.render(self.titulo, True, c.BLANCO)
        texto1 = self.fuentes.render(self.texto, True, c.BLANCO)
        texto2 = self.fuentes.render(self.texto2, True, c.BLANCO)
        texto3 = self.fuentes.render(self.titulo, True, c.NEGRO)
        texto4 = self.fuentes.render(self.texto, True, c.NEGRO)
        texto5 = self.fuentes.render(self.texto2, True, c.NEGRO)
        self.ventana.blit(c.ItemSheets["timer"].convert_alpha(), (390,-17))

        text = self.fuenteTime.render( "{} : {}".format(self.min, self.seg), True, c.BLANCO )
        self.ventana.blit(text, (445, 20))
        
        self.ventana.blit(texto3, (747, 73))
        self.ventana.blit(texto4, (747, 103))
        self.ventana.blit(texto5, (747, 123))
        self.ventana.blit(texto, (750, 70))
        self.ventana.blit(texto1, (750, 100))
        self.ventana.blit(texto2, (747, 120))



